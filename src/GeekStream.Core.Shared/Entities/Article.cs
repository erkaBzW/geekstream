﻿using System;
using System.Collections.Generic;

namespace GeekStream.Core.Shared.Entities
{
    public class Article
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public string Content { get; set; }
        public int Rating { get; set; }
        public DateTime PublishedDate { get; set; }

        public int CategoryId { get; set; }
        public Category Category { get; set; }

        public string AuthorId { get; set; }
        public ApplicationUser Author { get; set; }

        public ICollection<Comment> Comments { get; set; }
    }
}
