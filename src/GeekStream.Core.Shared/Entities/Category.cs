﻿using System;
using System.Collections.Generic;

namespace GeekStream.Core.Shared.Entities
{
    public class Category
    {
        public int Id { get; set; }
        public string Name { get; set; }

        public ICollection<Article> Articles { get; set; }
    }
}
