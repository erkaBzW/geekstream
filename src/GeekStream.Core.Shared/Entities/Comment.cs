﻿using System;
using System.Collections.Generic;

namespace GeekStream.Core.Shared.Entities
{
    public class Comment
    {
        public int Id { get; set; }
        public string Content { get; set; }

        public string AuthorId { get; set; }
        public ApplicationUser Author { get; set; }

        public int ArticleId { get; set; }
        public Article Article { get; set; }
    }
}
