﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GeekStream.Core.Shared.Entities;

namespace GeekStream.Core.Shared.Interfaces.DataAccess
{
    public interface IArticleRepository : IDisposable
    {
        IEnumerable<Article> GetArticles();
        Task<Article> GetArticleById(int? articleId);
        Task InsertArticle(Article article);
        Task DeleteArticle(int articleId);
        Task UpdateArticle(Article article);
        IEnumerable<Article> SelectByCategoryId(int? categoryId);
    }
}
