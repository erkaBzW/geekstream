﻿using System.Collections.Generic;
using System.Linq;
using GeekStream.Core.Shared.Interfaces.DataAccess;
using GeekStream.Core.ViewModels;
using System.Threading.Tasks;
using GeekStream.Core.Shared.Entities;
using System;
using System.Security.Claims;

namespace GeekStream.Core.Services
{
    public class ArticlesService
    {
        private readonly IArticleRepository _articleRepository;
        private readonly UsersService _usersService;

        public ArticlesService(IArticleRepository articleRepository, UsersService usersService)
        {
            _articleRepository = articleRepository;
            _usersService = usersService;
        }

        public IEnumerable<ArticleViewModel> GetArticles()
        {
            return _articleRepository.GetArticles()
                .Select(article => new ArticleViewModel
                {
                    Id = article.Id,
                    Title = article.Title,
                    Content=article.Content,
                    Rating=article.Rating,
                    PublishedDate=article.PublishedDate,
                    Category = article.Category.Name,
                    User = article.Author.UserName
                });
        }

        public async Task SaveArticle(ArticleCreationViewModel model, ClaimsPrincipal User)
        {
            var article = new Article
            {
                Title = model.Title,
                Content = model.Content,
                Rating=0,
                PublishedDate=DateTime.UtcNow,
                CategoryId=model.CategoryId,
                AuthorId=_usersService.GetCurrentUser(User).Id
            };
            await _articleRepository.InsertArticle(article);
        }

        public async Task<ArticleViewModel> GetArticleById(int? articleId)
        {
            var article = await _articleRepository.GetArticleById(articleId);
            return new ArticleViewModel
            {
                Id = article.Id,
                Title = article.Title,
                Content = article.Content,
                Rating = article.Rating,
                PublishedDate = article.PublishedDate,
                Category = article.Category.Name,
                User=article.Author.UserName
            };
        }
        public IEnumerable<ArticleViewModel> SelectByCategoryId(int? id)
        {
            return _articleRepository.SelectByCategoryId(id)
            .Select(article => new ArticleViewModel
            {
                Id = article.Id,
                Title = article.Title,
                Content = article.Content,
                Rating = article.Rating,
                PublishedDate = article.PublishedDate,
                Category = article.Category.Name,
                User = article.Author.UserName
            });
        }
    }
}
