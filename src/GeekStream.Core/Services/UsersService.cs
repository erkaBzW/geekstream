﻿using System.Collections.Generic;
using GeekStream.Core.Shared.Interfaces.DataAccess;
using GeekStream.Core.Shared.Entities;
using Microsoft.AspNetCore.Identity;
using System.Security.Claims;

namespace GeekStream.Core.Services
{
    public class UsersService
    {
        private readonly IUserRepository _userRepository;
        private readonly UserManager<ApplicationUser> _userManager;


        public UsersService(IUserRepository userRepository,
                            UserManager<ApplicationUser> userManager)
        {
            _userRepository = userRepository;
            _userManager = userManager;
        }

        public ApplicationUser GetCurrentUser(ClaimsPrincipal User)
        {
            return _userManager.GetUserAsync(User).Result;
        }

        public IEnumerable<ApplicationUser> GetUsers()
        {
            return _userRepository.GetUsers();
        }
    }
}
