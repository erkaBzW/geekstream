﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GeekStream.Core.Shared.Interfaces.DataAccess;
using GeekStream.Core.Shared.Entities;
using Microsoft.EntityFrameworkCore;

namespace GeekStream.Infrastructure.DataAccess
{
    public class ArticleRepository : IArticleRepository
    {
        private readonly AppDbContext _context;
        public ArticleRepository(AppDbContext context)
        {
            _context = context;
        }
        public IEnumerable<Article> GetArticles()
        {
            return _context.Articles
                .Include(article=>article.Category)
                .Include(article => article.Author)
                .ToList();
        }
        public async Task<Article> GetArticleById(int? articleId)
        {
            return await _context.Articles
                .Include(article => article.Category)
                .Include(article => article.Author)
                .FirstOrDefaultAsync(article => article.Id == articleId);
        }
        public IEnumerable<Article> GetArticles(int page, int pageSize)
        {
            return _context.Articles
                .Include(article => article.Category)
                .Include(article => article.Author)
                .Skip((page - 1) * pageSize)
                .Take(pageSize)
                .ToList();
        }
        public async Task InsertArticle(Article article)
        {
            _context.Articles.Add(article);
            await _context.SaveChangesAsync();
        }

        public async Task UpdateArticle(Article article)
        {
            _context.Entry(article).State = EntityState.Modified;
            await _context.SaveChangesAsync();
        }
        public async Task DeleteArticle(int articleId)
        {
            var article = await GetArticleById(articleId);
            if (article != null)
            {
                _context.Articles.Remove(article);
                await _context.SaveChangesAsync();
            }
        }
        public IEnumerable<Article> SelectByCategoryId(int? categoryId)
        {
            return _context.Articles
                .Include(article => article.Category)
                .Include(article => article.Author)
                .Where(article=>article.CategoryId==categoryId)
                .ToList();
        }


        private bool _disposed = false;
        protected virtual void Dispose(bool disposing)
        {
            if (!this._disposed)
            {
                if (disposing)
                {
                    _context.Dispose();
                }
            }
            this._disposed = true;
        }
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}
