﻿using GeekStream.Core.Shared.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace GeekStream.Infrastructure.Mappings
{
    public class ApplicationUserMap : IEntityTypeConfiguration<ApplicationUser>
    {
        public void Configure(EntityTypeBuilder<ApplicationUser> builder)
        {
            builder.Property(x => x.FirstName)
                .HasColumnType("nvarchar(150)")
                .IsRequired();

            builder.Property(x => x.LastName)
                .HasColumnType("nvarchar(250)")
                .IsRequired();

            builder.HasMany(x => x.Articles)
                .WithOne(x => x.Author)
                .HasForeignKey(x => x.AuthorId);

            builder.HasMany(x => x.Comments)
                .WithOne(x => x.Author)
                .HasForeignKey(x => x.AuthorId);

            builder.HasMany(x => x.ApplicationUserRoles)
                .WithOne(x => x.User)
                .HasForeignKey(x => x.RoleId);
        }
    }
}